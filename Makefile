MODULE_NAME := RiemannProblem

MAJOR := 0
MINOR := 0
PATCH := 0


OBJS := allocate_rp_star_region_SoA.o


TEST_OBJS := allocate_rp_star_region_SoA_tests.o \
             riemann_problem_tests.o


LIBS :=
paCages :=


include ./Makefile.paCage/Makefile
