/*
 * allocate_rp_star_region_SoA.c
 * tests_file: allocate_rp_star_region_SoA_tests.c
 *
 * Allocating a structure of arrays of rp_star_region variables with a given
 * length
 *
 * @param: s: A pointer to a given rp_star_region_SoA structure
 * @param: len: The length of arrays
 * @param: allocate: Allocation function
 *
 * @return: void
 */


#include <stdlib.h>
#include "./allocate_rp_star_region_SoA.h"


void allocate_rp_star_region_SoA(rp_star_region_SoA_t *s,
    int len, void allocate(int, size_t, void **))
{
  allocate(len, sizeof(double), (void **)&(s->p));
  allocate(len, sizeof(double), (void **)&(s->u));
  allocate(len, sizeof(double), (void **)&(s->rhol));
  allocate(len, sizeof(double), (void **)&(s->rhor));
  s->len = len;
}
