#ifndef _ALLOCATE_RP_STAR_REGION_SOA_H_
#define _ALLOCATE_RP_STAR_REGION_SOA_H_


#include <stdlib.h>
#include "./riemann_problem.h"


#ifdef __cplusplus
extern "C" {
#endif

void allocate_rp_star_region_SoA(rp_star_region_SoA_t *, int,
    void allocate(int, size_t, void **));

#ifdef __cplusplus
}
#endif


#endif /* _ALLOCATE_RP_STAR_REGION_SOA_H_ */
