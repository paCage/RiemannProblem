#ifndef _RIEMANN_PROBLEM_H_
#define _RIEMANN_PROBLEM_H_


#ifdef __cplusplus
extern "C" {
#endif


#include <stdbool.h>


#define RP_STAR_REGION_T_LEN (4)


/* Shock structure */
typedef struct _rp_shock_t {
  double rho, speed;
} rp_shock_t;

/* Rarefaction structure */
typedef struct _rp_rarefaction_t {
  double rho, cs, speed_H, speed_T;
} rp_rarefaction_t;

/* Side state type */
typedef struct _rp_side_t {
  bool is_shock;

  union {
    rp_shock_t shock;
    rp_rarefaction_t rarefaction;
  };
} rp_side_t;


/* Riemann Problem Star Region type */
typedef struct _rp_star_region_t {
  double p, u;
  rp_side_t left;
  rp_side_t right;
} rp_star_region_t;


/* Riemann Problem Star Region type (structure of arrays) */
typedef struct {
  int len;
  double *p;
  double *u;
  double *rhol;
  double *rhor;
  /* TODO: Added shock/rarefaction members here */
} rp_star_region_SoA_t;


#ifdef __cplusplus
}
#endif


#endif /* _RIEMANN_PROBLEM_H_ */
