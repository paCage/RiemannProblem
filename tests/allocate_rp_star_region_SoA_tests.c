/*
 * allocate_rp_star_region_SoA_tests.c
 */


#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>
#include "./../src/allocate_rp_star_region_SoA.h"

#define LEN 13

void mocked_allocate_h(int len, size_t size, void **p) {
    mock(len, size, p);
}


Describe(allocate_rp_star_region_SoA);
BeforeEach(allocate_rp_star_region_SoA) {};
AfterEach(allocate_rp_star_region_SoA) {};


Ensure(allocate_rp_star_region_SoA,
    calls_the_allocation_function_and_sets_the_length)
{
  rp_star_region_SoA_t s;

  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));
  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));
  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));
  expect(mocked_allocate_h, when(len, is_equal_to(LEN)));

  allocate_rp_star_region_SoA(&s, LEN, mocked_allocate_h);

  assert_that(s.len, is_equal_to(LEN));
}
