/*
 * riemann_problem_tests.c
 */


#include <cgreen/cgreen.h>
#include <stdbool.h>
#include "./../src/riemann_problem.h"


Describe(riemann_problem);
BeforeEach(riemann_problem) {};
AfterEach(riemann_problem) {};


Ensure(riemann_problem, has_correct_data_structure)
{
  rp_star_region_t star;

  star.p = 1.23;
  assert_that_double(star.p, is_equal_to_double(1.23));

  star.u = 2.34;
  assert_that_double(star.u, is_equal_to_double(2.34));

  star.left.is_shock = true;
  assert_true(star.left.is_shock);

  star.left.shock.rho = 3.45;
  assert_that_double(star.left.shock.rho, is_equal_to_double(3.45));

  star.left.shock.speed = 4.56;
  assert_that_double(star.left.shock.speed, is_equal_to_double(4.56));

  star.right.is_shock = false;
  assert_false(star.right.is_shock);

  star.right.rarefaction.rho = 5.67;
  assert_that_double(star.right.rarefaction.rho, is_equal_to_double(5.67));

  star.right.rarefaction.cs = 6.78;
  assert_that_double(star.right.rarefaction.cs, is_equal_to_double(6.78));

  star.right.rarefaction.speed_H = 7.89;
  assert_that_double(star.right.rarefaction.speed_H, is_equal_to_double(7.89));

  star.right.rarefaction.speed_T = 8.90;
  assert_that_double(star.right.rarefaction.speed_T, is_equal_to_double(8.90));
}
